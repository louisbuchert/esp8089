/*
 * Copyright (c) 2011-2014 Espressif System.
 *
 * esp debug
 */

#ifndef _DEBUG_H_

#ifdef ASSERT_PANIC
#define ESSERT(v) BUG_ON(!(v))
#else
#define ESSERT(v) if(!(v)) printk("ESSERT:%s %d\n", __FILE__, __LINE__)
#endif

#include <linux/slab.h>
#include <linux/debugfs.h>
#include <linux/uaccess.h>

enum esp_type {
	ESP_BOOL,
	ESP_U8,
	ESP_U16,
	ESP_U32,
	ESP_U64
};

void esp_dump_var(const char *name, struct dentry *parent,
			    void *value, enum esp_type type);

struct dentry *esp_dump_array(const char *name, struct dentry *parent,
			      struct debugfs_blob_wrapper *blob);

struct dentry *esp_dump(const char *name, struct dentry *parent, void *data,
			int size);

struct dentry *esp_debugfs_add_sub_dir(const char *name);

int esp_debugfs_init(void);

void esp_debugfs_exit(void);

enum {
	ESP_DBG_ERROR = BIT(0),
	ESP_DBG_TRACE = BIT(1),
	ESP_DBG_LOG = BIT(2),
	ESP_DBG = BIT(3),
	ESP_SHOW = BIT(4),
	ESP_DBG_TXAMPDU = BIT(5),
	ESP_DBG_OP = BIT(6),
	ESP_DBG_PS = BIT(7),
	ESP_ATE = BIT(8),
	ESP_DBG_ALL = GENMASK(31, 0)
};

extern unsigned int esp_msg_level;

#define esp_dbg(mask, fmt, args...) do {                  \
	if (esp_msg_level & mask)                         \
	printk(fmt, ##args);                          \
} while (0)

void show_buf(u8 *buf, u32 len);

#endif				/* _DEBUG_H_ */
